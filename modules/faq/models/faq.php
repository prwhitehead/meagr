<?

namespace Modules\Faq\Models;

class Faq extends \Meagr\Model {

	public static $table = 'faq';
	
	public static $columns = array(
			'id' => array(
				'database' => array(
						'id' => 'id', 
						'type' => 'int(10)', 
						'primary' => true, 
						'auto_increment' => true, 
						'null' => false
					),
				'form' => array(
						'display' => true, 
						'title' => 'ID', 
						'type' => 'readonly',
						'validate' => array()
					),
				'list' => array(
						'display' => true, 
						'title' => 'ID', 
					),				
				),
			'new_id' => array(
				'database' => array(
						'id' => 'new_id', 
						'type' => 'int(10)', 
					),
				'form' => array(
						'display' => true, 
						'title' => 'ID', 
						'type' => 'readonly',
						'validate' => array()
					),
				'list' => array(
						'display' => true, 
						'title' => 'ID', 
					),				
				),		
			'question' => array(
				'database' => array(
						'id' => 'question', 
						'type' => 'varchar(255)', 
						'default' => '', 
						'null' => false
					),
				'form' => array(
						'display' => true, 
						'title' => 'Title', 
						'type' => 'text',
						'validate' => array('not_empty')					
					),
				'list' => array(
						'display' => true, 
						'title' => 'Title', 
					),				
				),
			'slug' => array(
				'database' => array(
						'id' => 'slug', 
						'type' => 'varchar(255)', 
						'default' => '', 
						'null' => false
					),
				'form' => array(
						'display' => true, 
						'title' => 'Title', 
						'type' => 'text',
						'validate' => array('not_empty', 'valid_url')					
					),
				'list' => array(
						'display' => true, 
						'title' => 'Title', 
					),				
				),
			'meta_title' => array(
				'database' => array(
						'id' => 'meta_title', 
						'type' => 'varchar(255)',
						'default' => '', 
						'null' => false
					),
				'form' => array(
						'display' => true, 
						'title' => 'Slug', 
						'type' => 'text',
						//pass to function not_ and pass in 'empty'
						//pass to function valid_ and pass in 'url'
						'validate' => array('not_empty', 'valid_url')					
					),				
				'list' => array(
						'display' => true, 
						'title' => 'Slug', 
					),				
				),
			'meta_desc' => array(
				'database' => array(
						'id' => 'meta_desc', 
						'type' => 'varchar(255)',
						'default' => '', 
						'null' => false
					),
				'form' => array(
						'display' => true, 
						'title' => 'Slug', 
						'type' => 'text',
						//pass to function not_ and pass in 'empty'
						//pass to function valid_ and pass in 'url'
						'validate' => array('not_empty', 'valid_url')					
					),				
				'list' => array(
						'display' => true, 
						'title' => 'Slug', 
					),				
				),		
			'content' => array(
				'database' => array(
						'id' => 'content', 
						'length' => '', 
						'type' => 'text',
						'default' => '', 
						'null' => true
					),
				'form' => array(
						'display' => true, 
						'title' => 'Slug', 
						'type' => 'text',
						'validate' => array()					
					),
				'list' => array(
						'display' => true, 
						'title' => 'Content', 
					),				
				),	
			'excerpt' => array(
				'database' => array(
						'id' => 'excerpt', 
						'length' => '', 
						'type' => 'text',
						'default' => '', 
						'null' => true
					),
				'form' => array(
						'display' => true, 
						'title' => 'Slug', 
						'type' => 'text',
						'validate' => array()					
					),
				'list' => array(
						'display' => true, 
						'title' => 'Content', 
					),				
				),				
			'parent' => array(
				'database' => array(
						'id' => 'parent', 
						'type' => 'int(10)',
						'default' => 0, 
						'null' => true
					),
				'form' => array(
						'display' => true, 
						'title' => 'Parent', 
						'type' => 'text',
						//pass to function is_ with value 'int'
						'validate' => array('is_int')					
					),
				'list' => array(
						'display' => true, 
						'title' => 'Parent', 
					),				
				),	
			'created_at' => array(
				'database' => array(
						'id' => 'created_at', 
						'length' => '', 
						'type' => 'datetime',
						'default' => '0000-00-00 00:00:00', 
						'null' => false
					),
				'form' => array(
						'display' => false, 
						'title' => 'Created', 
						'type' => 'readonly'				
					),
				'list' => array(
						'display' => true, 
						'title' => 'Created', 
					),				
				),	
			'viewed_at' => array(
				'database' => array(
						'id' => 'viewed_at', 
						'length' => '', 
						'type' => 'datetime',
						'default' => '0000-00-00 00:00:00', 
						'null' => false
					),
				'form' => array(
						'display' => false, 
						'title' => 'Created', 
						'type' => 'readonly'				
					),
				'list' => array(
						'display' => true, 
						'title' => 'Created', 
					),				
				),				
			'updated_at' => array(
				'database' => array(
						'id' => 'updated_at', 
						'length' => '', 
						'type' => 'datetime',
						'default' => '0000-00-00 00:00:00', 
						'null' => false
					),
				'form' => array(
						'display' => false, 
						'title' => 'Last updated', 
						'type' => 'readonly',
						'validate' => array()					
					),
				'list' => array(
						'display' => false, 
						'title' => 'Updated', 
					),				
				),																						
		);

	static function test() {
		p(__METHOD__);
	}
}