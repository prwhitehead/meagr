<?

namespace Modules\Models;

class Member extends \Meagr\Model {

	public static $table = 'member';
	
	public static $columns = array(
			'id' => array(
				'database' => array(
						'id' => 'id', 
						'type' => 'int(10)', 
						'primary' => true, 
						'auto_increment' => true, 
						'null' => false
					),
				'form' => array(
						'display' => true, 
						'title' => 'ID', 
						'type' => 'readonly',
						'validate' => array()
					),
				'list' => array(
						'display' => true, 
						'title' => 'ID', 
					),				
				),
			'email' => array(
				'database' => array(
						'id' => 'email', 
						'type' => 'varchar(255)', 
					),
				'form' => array(
						'display' => true, 
						'title' => 'Email', 
						'type' => 'text',
						'validate' => array()
					),
				'list' => array(
						'display' => true, 
						'title' => 'ID', 
					),				
				),		
			'password' => array(
				'database' => array(
						'id' => 'password', 
						'type' => 'varchar(255)', 
						'default' => '', 
						'null' => false
					),
				'form' => array(
						'display' => true, 
						'title' => 'password', 
						'type' => 'text',
						'validate' => array('not_empty')					
					),
				'list' => array(
						'display' => true, 
						'title' => 'Title', 
					),				
				),
			'salt' => array(
				'database' => array(
						'id' => 'salt', 
						'type' => 'varchar(255)', 
						'default' => '', 
						'null' => false
					),
				'form' => array(
						'display' => false, 
						'title' => 'salt', 
						'type' => 'text',
						// 'validate' => array('not_empty', 'valid_url')					
					),
				'list' => array(
						'display' => false, 
						'title' => 'Title', 
					),				
				),
			'first_name' => array(
				'database' => array(
						'id' => 'first_name', 
						'type' => 'varchar(255)',
						'default' => '', 
						'null' => false
					),
				'form' => array(
						'display' => true, 
						'title' => 'First Name', 
						'type' => 'text',
						//pass to function not_ and pass in 'empty'
						//pass to function valid_ and pass in 'url'
						'validate' => array('not_empty')					
					),				
				'list' => array(
						'display' => true, 
						'title' => 'First Name', 
					),				
				),
			'last_name' => array(
				'database' => array(
						'id' => 'last_name', 
						'type' => 'varchar(255)',
						'default' => '', 
						'null' => false
					),
				'form' => array(
						'display' => true, 
						'title' => 'Last Name', 
						'type' => 'text',
						//pass to function not_ and pass in 'empty'
						//pass to function valid_ and pass in 'url'
						// 'validate' => array('not_empty', 'valid_url')					
					),				
				'list' => array(
						'display' => true, 
						'title' => 'Last Name', 
					),				
				),		

			'created_at' => array(
				'database' => array(
						'id' => 'created_at', 
						'length' => '', 
						'type' => 'datetime',
						'default' => '0000-00-00 00:00:00', 
						'null' => false
					),
				'form' => array(
						'display' => false, 
						'title' => 'Created', 
						'type' => 'readonly'				
					),
				'list' => array(
						'display' => true, 
						'title' => 'Created', 
					),				
				),				
			'updated_at' => array(
				'database' => array(
						'id' => 'updated_at', 
						'length' => '', 
						'type' => 'datetime',
						'default' => '0000-00-00 00:00:00', 
						'null' => false
					),
				'form' => array(
						'display' => false, 
						'title' => 'Last updated', 
						'type' => 'readonly',
						'validate' => array()					
					),
				'list' => array(
						'display' => false, 
						'title' => 'Updated', 
					),				
				),																						
		);

	static function test() {
		return true;
	}
}