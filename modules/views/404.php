<section id="home">
	<header class="hero-unit">
		<h1>404!</h1>
		<p>Bad things happened and you're lost... </p>
	</header>
	<article id="home-content">
		
		<p>The file you are currently reading can be found here: </p>
		<code><?=__FILE__; ?></code>
		<p>Your current environment is set to:</p>
		<code><?=ENVIRONMENT; ?></code>

		<? if (isset($backtrace) and ! empty($backtrace)) : ?>
			<strong>Backtrace</strong>
			<table class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Class::Function</th>
						<th>File</th>
						<th>Line</th>
					</tr>
				</thead>
				<tbody>					
					<? foreach($backtrace as $k => $v) { ?>
						<tr>
							<td><?="#".$k; ?></td>
							<td>
								<?=(isset($v['class']) ? $v['class'] . '::' : '') . $v['function']; ?>
								<? if ($v['function'] == 'call_user_func_array') { ?>
									<? $args = implode('::', $v['args'][0]); ?>
									<small><?=' - ' . $args; ?></small>
								<? } ?>
							</td>
							<td><?=$v['file']; ?></td>
							<td><?=$v['line']; ?></td>
						</tr>
				    <? } ?> 
				</tbody>
			</table>
		<? endif; ?>
	</article>
</section>