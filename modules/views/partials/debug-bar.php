<div id="debug">
	<a href="#" class="toggle">#</a>
	<ul class="nav nav-tabs" id="debug-bar">
		<li class="active"><a href="#home">Home</a></li>
		<li><a href="#profile">Profile</a></li>
		<li><a href="#messages">Messages</a></li>
		<li><a href="#settings">Settings</a></li>
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="home">
			<?p(\Meagr\Debug::$debug_backtrace); ?>

		</div>
		<div class="tab-pane" id="profile">profile</div>
		<div class="tab-pane" id="messages">message</div>
		<div class="tab-pane" id="settings">settings</div>
	</div>

	<script>
		$(function() {
			$('#debug-bar a').click(function(e) {
				e.preventDefault();
				$(this).tab('show');
			});

			$('#debug .toggle').click(function(){
				var $this = $(this); 

				var tab = $this.parent().find('.tab-content');
				if (tab.is(':visible')) {
					tab.hide();
				} else {
					tab.show();
				}

				return false;
			});
	    });
	</script>
</div>