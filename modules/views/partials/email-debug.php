<div style="width:960px; margin: 50px auto; height:auto;">

	<!-- content -->
	<table class="table table-striped table-bordered">
		<caption>Email</caption>
	</table>	

	<div style="width:800px; margin: 50px auto; height:auto;">
		<?=$object->content; ?>		
	</div>

	<!-- Arguemnts-->
	<? $data = $object->debug_backtrace; ?>
	<? if (! empty($data)) { ?>
		<table class="table table-striped table-bordered">
			<caption>Debug Backtrace</caption>
			<thead>
				<tr>
					<th>Key</th>
					<th>Value</th>
				</tr>
			</thead>
			<tbody>
				<? foreach($data as $k => $v) { ?>
					<? if (is_array($v)) continue; ?>
					<tr>	
						<td><?=$k; ?></td>
						<td><?=$v; ?></td>
					</tr>	
				<? } ?>	
			</tbody>	
		</table>			
	<? } ?>

	<!-- Arguemnts-->
	<? $data = $object->content_args; ?>
	<? if (! empty($data)) { ?>
		<table class="table table-striped table-bordered">
			<caption>Content Variables</caption>
			<thead>
				<tr>
					<th>Key</th>
					<th>Value</th>
				</tr>
			</thead>
			<tbody>
				<? foreach($data as $k => $v) { ?>
					<? if (is_array($v)) continue; ?>
					<tr>	
						<td><?=$k; ?></td>
						<td><?=$v; ?></td>
					</tr>	
				<? } ?>	
			</tbody>	
		</table>			
	<? } ?>

	<!-- File locations -->
	<table class="table table-striped table-bordered">
		<caption>Included files</caption>
		<? $types = array('header', 'footer'); ?>
		<thead>
			<tr>
				<th>Name</th>
				<th>File Location</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Header File</td>
				<td><?=$object->header; ?></td>
			</tr>
			<tr>
				<td>Footer File</td>
				<td><?=$object->footer; ?></td>
			</tr>		
		</tbody>	
	</table>	

	<!-- File locations -->
	<table class="table table-striped table-bordered">
		<caption>Attached Files</caption>
		<? $types = array('header', 'footer'); ?>
		<thead>
			<tr>
				<th>File Location</th>
			</tr>
		</thead>
		<tbody>
			<? foreach($object->attachment as $attachment) { ?>
				<tr>
					<td><?=$attachment; ?></td>
				</tr>	
			<? } ?>	
		</tbody>	
	</table>


	<!-- addresses being used -->
	<? $types = array('addresses', 'bcc', 'cc'); ?>
	<? foreach($types as $type) { ?>
		<? $data = $object->{$type}; ?>
		<? if (! empty($data)) { ?>
			<? $header = array_keys($data[0]); ?>
			<table class="table table-striped table-bordered">
				<caption><?=$type; ?></caption>
				<thead>
					<tr>
						<? foreach($header as $th) { ?>
							<th><?=$th; ?></th>
						<? } ?> 
					</tr>
				</thead>
				<tbody>
					<tr>
						<? foreach($data as $d) { ?>
							<td><?=$d['name']; ?></td>
							<td><?=$d['email']; ?></td>
						<? } ?>
					</tr>
				</tbody>	
			</table>			
		<? } ?>		
	<? } ?>

	<!-- email data being used -->
	<? $data = $object->email; ?>
	<? if (! empty($data)) { ?>
		<? unset($data->Body); ?>
		<table class="table table-striped table-bordered">
			<caption>PHPMailer</caption>
			<thead>
				<tr>
					<th>Key</th>
					<th>Value</th>
				</tr>
			</thead>
			<tbody>
				<? foreach($data as $k => $v) { ?>
					<? if (is_array($v)) continue; ?>
					<tr>	
						<td><?=$k; ?></td>
						<td><?=$v; ?></td>
					</tr>	
				<? } ?>	
			</tbody>	
		</table>			
	<? } ?>
</div>