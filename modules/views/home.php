<section id="home">
	<header class="hero-unit" style="background:url(<?=PUBLIC_URL . '/images/collage.jpg'; ?>) top center repeat; ?>">
		<h1>Meagr</h1>
		<h3>small and cheap</h3>
	</header>
	<article id="home-content">
		<p>Welcome to Meagr. This is the default view which can be found and edited here:</p>
		<code><?=__FILE__; ?></code>
		<p>The view file is controlled and called from the controller class which is currently set to be:</p>
		<code><?=$controller; ?></code>
		<p>Your current environment is set to:</p>
		<code><?=ENVIRONMENT; ?></code>
		<p>Inorder to change the environment and so use a new config file you can set the ENVIRONMENT constant within</p>
		<code><?=SITE_PATH; ?>/public/index.php</code>
		<p>To edit and change the way your setup works and to make changes to options and defaults, edit the site config which, based on your current environment, can be found in:</p>
		<code><?=SITE_PATH; ?>/app/config/<?=ENVIRONMENT; ?>/config.php</code>
		<p>For more information, please read the detailed README file</p>
	</article>
</section>

<strong>Current Log</strong>
<p class="small">{log}</p>

<strong>Current Files</strong>
<p class="small">{file}</p>