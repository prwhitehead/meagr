<?

namespace Modules\Admin\Controllers; 

class Admin extends \Meagr\Controller {

	protected static $app = 'admin';

	private static function check() {
		if (! Meagr\Auth::check()) {  
			Meagr\Router::redirect('/' . self::$app . '/login');			
		}
	}

	public static function GET_index() {
		//redirect the root /admin/ to /admin/dashboard
		 Meagr\Router::redirect('/' . self::$app . '/dashboard');
	}


	// public static function GET_user() {
	// 	//get the form from the method
	// 	$form = self::FORM_register(); 

	// 	//otherwise include the form
	// 	Meagr\View::view('Admin::register', compact('form'));
	// }	

	//our dashboard
	public static function GET_dashboard() {
		self::check(); 
	}

	public static function GET_logout() {
		Meagr\Auth::destroy(); 
		self::check();
	}

	public static function GET_register() {

		//get the form from the method
		$form = self::FORM_register(); 

		//otherwise include the form
		Meagr\View::view('Admin::register', compact('form'));
	}

	public static function POST_register() {

		//redirect to the dash if the user is logged in
		if (Meagr\Auth::check()) {
			Meagr\Router::redirect('/' . self::$app . '/dashboard');
		}

		$form = self::FORM_register();

		if($form->valid()) {
			Meagr\Router::redirect('/' . self::$app . '/register?passed');
		} else {
			Meagr\Router::redirect('/' . self::$app . '/register?failed');
		}
	}

	public static function FORM_register() {

		$form = Meagr\Form::init(array(
			'id' => 'login', 
			'class' => 'standard-form',
			// 'horizonal' => true, 
			'method' => 'post', 
			'action' => '/' . self::$app . '/register', 
			'_nonce' => 'login'
		)); 

		$form->addHTML('<h2>Register</h2>'); 

		$form->addFields(
			array(
				'first_name' => array(
					'id' => 'first_name', 
					'name' => 'first_name', 
					'class' => 'form-element input-large', 
					'label' => 'First name', 
					'help' => '',
					'placeholder' => 'John', 
					'type' => 'text',
					'validate' => array(
						'not' => array('empty' => 'A first name musst be provided')
						), 
					'value' => '', 				
				), 
				'last_name' => array(
					'id' => 'last_name', 
					'name' => 'last_name', 
					'class' => 'form-element', 
					'label' => 'Last name', 
					'help' => '',
					'placeholder' => 'Smith', 
					'type' => 'text',
					'validate' => array(
						'not' => array('empty' => 'A last name must be provided')
						),				
					'value' => ''
				), 								
				'email' => array(
					'id' => 'email', 
					'name' => 'email', 
					'class' => 'form-element ', 
					'label' => 'Email Address', 
					'help' => '',
					'placeholder' => 'john@smith.com', 
					'type' => 'text',
					'validate' => array(
						'not' => array('empty' => 'A valid email address must be provided'), 
						'valid' => array('email' => 'A valid email address must be provided')
						),
					'value' => '' 				
				), 				
				'password' => array(
					'id' => 'password', 
					'name' => 'password', 
					'class' => 'form-element input-large', 
					'label' => 'Password', 
					'type' => 'password',
					'validate' => array(
						'not' => array('empty' => 'A password must be provided')
						),					
					'value' => ''
				), 
				'password2' => array(
					'id' => 'password2', 
					'name' => 'password2', 
					'class' => 'form-element input-large', 
					'label' => 'Confirm password', 
					'type' => 'password',
					'validate' => function() { 
							$errors = array();

							if (trim(Meagr\Input::post('password2')) == '') {
								$errors[] = 'Password Confitmation must not be empty';
							}

							if ((Meagr\Input::post('password') !== Meagr\Input::post('password2'))) {
								$errors[] = 'Password Confitmation must match password ';
							} 

							if (! empty($errors)) {
								return $errors;
							}

							//if we get 
							return true;
						},
					'value' => '', 				
				),									
				'submit' => array(
					'id' => 'submit', 
					'name' => 'submit', 
					'class' => 'form-element', 
					'label' => false, 
					'type' => 'submit',
					'value' => 'Submit',
					'primary' => true, 
				), 
				'cancel' => array(
					'id' => 'cancel', 
					'name' => '', 
					'class' => 'form-element ', 
					'label' => '', 
					'type' => 'link',
					'href' => Meagr\Router::redirect('/admin', false),
					'value' => 'Cancel',
					'primary' => false
				)
			)
		); 
		
		return $form;		
	}


	public static function GET_login() {
		//redirect to the dash if the user is logged in
		if (Meagr\Auth::check()) {
			Meagr\Router::redirect('/' . self::$app . '/dashboard');
		}

		$form = Meagr\Form::init(array(
			'id' => 'login', 
			'class' => 'standard-form',
			'horizonal' => false, 
			'method' => 'post', 
			'action' => '/' . self::$app . '/login', 
			'_nonce' => 'login'
		)); 

		$form->addHTML('<h2>Login</h2>'); 

		$form->addFields(
			array(
				'email' => array(
					'id' => 'email', 
					'name' => 'email', 
					'class' => 'form-element input-large', 
					'label' => 'Email Address', 
					'help' => 'Please enter a valid email address',
					'placeholder' => 'john@smith.com', 
					'type' => 'email',
					'append' => array(

						),
					'prepend' => array(

						),
					'validate' => array(
						'not' => 'empty', 
						'is' => array('string', 'email')
					), 
					'value' => Meagr\Input::post('email')
				), 
				'password' => array(
					'id' => 'password', 
					'name' => 'password', 
					'class' => 'form-element input-large', 
					'label' => 'Password', 
					'type' => 'password',
					'validate' => array(
						'not' => 'empty', 
						'is' => array('string')
					), 
					'value' => ''
				), 
				'remember_me' => array(
					'id' => 'remember_me', 
					'name' => 'remember_me', 
					'class' => 'form-element input-medium', 
					'inline' => true, 
					'label' => '', 
					'type' => 'checkbox',
					'options' => array(
						'1' => 'Remeber me'
					), 
					'value' => 'no'
				), 				
				'submit' => array(
					'id' => 'submit', 
					'name' => 'submit', 
					'class' => 'form-element', 
					'label' => '', 
					'type' => 'submit',
					'value' => 'Submit'
				), 
			)
		); 

		//otherwise include the form
		Meagr\View::view('Admin::login', compact('form'));
	}

	//deal with authenticating our users, checking passwords and creating sessions
	public static function POST_login() { 
		//get our variables
		extract($_POST); 
		//check the nonce first
		if (! Meagr\Nonce::valid(Meagr\Input::post('_nonce'), 'login')) {
			Meagr\Router::redirect('/' . self::$app . '/login?bad-nonce');
		}

		//check for a match from the db based on their entered details
		$member = \Modules\Models\Member::init()->where('email', $email);
		//check the user exists first
		if (count($member)) { 
			//get the first index in results
			$member = $member[0]; 
			//init the Bcrypt class and set the instance's salt to the members db salt
			$crypt = new Meagr\Bcrypt(); 
			$crypt->setSalt($member->salt);
			//now we can check to see if the users password matches our hash with the users salt
			if ($crypt->hash($password) == $member->password) { 
				//the password matches, so create the session. 
				//we can get user data with Meagr\Auth::current() which returns a Model object of member data
				Meagr\Auth::create($member->id);
				Meagr\Router::redirect('/' . self::$app . '/dashboard');

			//if the passwords dont match, force logout	
			} else {
				Meagr\Auth::destroy();
				Meagr\Router::redirect('/member');
			}

		//the memeber couldnt be found
		} else {
			throw new Meagr\MeagrException('User not found');
		}
	}

	public static function GET_add() {

		$names = array('joohn', 'paul', 'liam', 'dan', 'sarah', 'greg');
		$last_name = array('qwerty', 'poerpo', 'iudfkjdfkj', 'kjdfkjd', 'oisdfkjhs');
		$emails = array('p@p.co.uk', 'qwoi@lklk.com', 'qoie@qw.com', 'qpaos@popo.com', 'sjsjs@pwpw.com');

		$b = new Meagr\Bcrypt(); 
		$salt = $b->getSalt();
		$password = $b->hash('123');

		$test = new Modules\Models\Member(); 
		$test->email = $emails[rand(0, count($emails)-1)];
		$test->password = $password; 
		$test->salt = $salt; 
		$test->first_name = $names[rand(0, count($names)-1)];
		$test->last_name = $last_name[rand(0, count($last_name)-1)];
		$id = $test->save();
	}
}