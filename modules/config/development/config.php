<?

namespace Modules\Config\Development; 

class Config extends \Meagr\Config {

	public static function member() {
		return array(
				'table' => 'member', 
				'password_col' => 'password', 
				'salt_col' => 'salt'
			);
	}	

	/**
	* Meta defaults 
	*
	* @return array
	*/
	public static function meta() {
		return array(
				'title' => __METHOD__, 
				'description' => 'Cheap, so you dont have to be',
				'keywords' => 'meagr, php, framework, one love, open source'
			);
	}	

	public static function routes() {
		return array(
				array('__HOME__' => '{modules}\{controllers}\Home::GET_index'),
				
				// //url => matching keywords
				// '/admin/login' => '\{Modules}\{Controllers}\Admin::GET_index2', 

				// //matching multiple url params
				// '/faq/{year}/{month}/{day}/%page%/{number}' => '\{Modules}\{class}::GET_archive', 

				// //simple slug => 
				// '/faq/{slug}' => '\{Modules}\Faq::GET_view',
			);
	}

	public static function database() {
		return array(
				'host' => 'localhost_', 
				'username' => 'root', 
				'password' => 'root', 
				'dbname' => 'test'
			);
	}

	public static function ftp() {
		return array(
				//default connection
				'default' => array(
					'host' => '', 
					'username' => '', 
					'password' => '',
					'port' => '21', 
					'passive' => true, 
					'exec' => false)				
			);
	}	

	public static function s3() {
		return array(
				'bucket_name' => 'test_bucket', 
				'key' => 'AKIAIWKD73NX3XRHVQ5A', 
				'secret' => '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'
			);
	}	

	public static function cache() {
		return array(
				'dir' => '/cache', //our cache dir
				'duration' => 60*60, //one hour 
			);
	}		
}