<?

namespace Modules\Config\Production; 

class Config extends \Meagr\Config {

	public static function routes() {
		return array(
				'__HOME__' => '\Modules\Controllers\Home::GET_index', 
				'__404__' => '\Modules\Controllers\Home::GET_notfound'
			);
	}

	public static function database() {
		return array(
				'host' => 'localhost', 
				'username' => 'root', 
				'password' => 'root', 
				'dbname' => 'test'
			);
	}
}