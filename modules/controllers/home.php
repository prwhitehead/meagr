<?

namespace Modules\Controllers; 

class Home extends \Meagr\Controller {

	public static function __before() {
		parent::__before();
		
	}

	public static function GET_index() {  
		\Meagr\View::view('home', array('controller' => __METHOD__));
	}
}