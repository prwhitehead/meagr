<?

namespace Modules\Member\Controllers;

class Member extends \Meagr\Controller {

	public static function GET_index() {
		p(__METHOD__);
	}

	public static function GET_dashboard() {
		if (Meagr\Auth::check()) {
			echo "you are logged in";
			
		} else {
			Meagr\Router::redirect('/member');
		}
	}

	public static function GET_logout() {
		Meagr\Auth::destroy(); 
		Meagr\Router::redirect('/member');
	}

	public static function GET_login($email = null, $password = null) {

		//check for a match from the db based on their entered details
		$member = \Modules\Models\Member::init()->where('email', $email);

		//check the user exists first
		if (count($member)) {

			//get the first index in results
			$member = $member[0]; 

			//init the Bcrypt class and set the instance's salt to the members db salt
			$crypt = new Meagr\Bcrypt(); 
			$crypt->setSalt($member->salt);

			//now we can check to see if the users password matches our hash with the users salt
			if ($crypt->hash($password) == $member->password) {
				//the password matches, so create the session. 
				//we can get user data with Meagr\Auth::current() which returns a Model object of member data
				Meagr\Auth::create($member->id);
				// Meagr\Router::redirect('/member/dashboard');

			//if the passwords dont match, force logout	
			} else {
				Meagr\Auth::destroy();
				Meagr\Router::redirect('/member');
			}

		//the memeber couldnt be found
		} else {
			throw new Meagr\MeagrException('User not found');
		}
	}

	public static function GET_add() {

		$names = array('joohn', 'paul', 'liam', 'dan', 'sarah', 'greg');
		$last_name = array('qwerty', 'poerpo', 'iudfkjdfkj', 'kjdfkjd', 'oisdfkjhs');
		$emails = array('p@p.co.uk', 'qwoi@lklk.com', 'qoie@qw.com', 'qpaos@popo.com', 'sjsjs@pwpw.com');

		$b = new Meagr\Bcrypt(); 
		$salt = $b->getSalt();
		$password = $b->hash('123');

		$test = new Modules\Models\Member(); 
		$test->email = $emails[rand(0, count($emails)-1)];
		$test->password = $password; 
		$test->salt = $salt; 
		$test->first_name = $names[rand(0, count($names)-1)];
		$test->last_name = $last_name[rand(0, count($last_name)-1)];
		$id = $test->save();
	}
}