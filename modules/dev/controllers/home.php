<?

namespace Modules\Dev\Controllers; 
use \Meagr\Core as Core; 

class Home extends \Meagr\Controller {

	public static function GET_index() {
		// echo Core\Log::all();

		p(__CLASS__, __METHOD__);
		
	}

	public static function GET_notfound() {
		echo '<strong>404</strong> Nothing found, sorry';
	}
}