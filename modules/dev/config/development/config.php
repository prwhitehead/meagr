<?

namespace Modules\Dev\Config\Development; 

class Config extends \Meagr\Config {

	public static function member() {
		return array(
				'table' => 'member', 
				'password_col' => 'password', 
				'salt_col' => 'salt'
			);
	}	

	public static function routes() {
		return array(
				'__HOME__' => '\Modules\Dev\Controllers\Home::GET_index', 
				'__404__' => '\Modules\Dev\Controllers\Home::GET_notfound'
			);
	}

	public static function database() {
		return array(
				'host' => 'localhost', 
				'username' => 'test', 
				'password' => 'test', 
				'dbname' => 'test'
			);
	}

	/**
	* FTP defaults and connections details
	*
	* @return array
	*/
	public static function ftp() {
		return array(
				//default connection
				'default' => array(
					'host' => 'ftp.prwhitehead.co.uk', 
					'username' => 'ftpprw@prwhitehead.co.uk', 
					'password' => Encrypt::decrypt('Fe0ldNJa7hVcRSCN2jrdeKoVZImE90KSpSynXqDkYJsB+5mcBf4ob135HC6J/s7YVkeD9fdK4c08Bu4UrPc85g=='),
					'port' => '21', 
					'passive' => true, 
					'exec' => false),
				//test	 
				'test' => array(
					'host' => 'ftp.prwhitehead.com', 
					'username' => 'ftpprw@prwhitehead.co.uk', 
					'password' => Encrypt::decrypt('Fe0ldNJa7hVcRSCN2jrdeKoVZImE90KSpSynXqDkYJsB+5mcBf4ob135HC6J/s7YVkeD9fdK4c08Bu4UrPc85g==')),
					'port' => '21'				
			);
	}


	/**
	* email defaults and SMTP connections details
	*
	* @return array
	*/
	public static function email() {
		return array(
				'header' => MODULE_PATH . '/views/partials/email-header.php', 
				'footer' => MODULE_PATH . '/views/partials/email-footer.php', 
				'from-address' => 'prwhitehead@gmail.com', 
				'from-name' => 'Test Testingsom', 
				'smtp' => false,
				'smtp-port' => '465', 
				'smtp-username' => 'prwhitehead@gmail.com', 
				'smtp-password' => Encrypt::decrypt('LQScrjgIlXBPd85JJWOJgoK5iH+1jYrgEiVd/Jl7BWErBKAKQoq6mHwfWXgCuHcenRbGmv2drbncgqvrlORFiA=='), 
				'smtp-host' =>'ssl://smtp.gmail.com'
			);
	}	
}