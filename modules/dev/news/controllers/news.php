<?

namespace Modules\Dev\News\Controllers;

class News extends \Meagr\Controller {

	public static function GET_index() { 
		p(__METHOD__);
		p(__CLASS__);
		$rows = Modules\Models\News::init()->limit(5)->orderDesc('created_at'); 
		\Meagr\View::view('news::list', array('rows' => $rows));
	}

	public static function GET_add() {
		for($i=1; $i<10; $i++) {
			$news = new Modules\Models\News(); 
			$content = 'Sed elementum facilisis tellus fringilla aliquet. Sed at molestie nunc. Vivamus laoreet risus nec augue malesuada porta. Aliquam eu augue sed turpis hendrerit dictum et ac tortor. Donec lobortis varius nisi id vestibulum. Suspendisse luctus lorem bibendum nibh vestibulum non suscipit justo pharetra. Ut arcu felis, feugiat et bibendum vel, bibendum ut ipsum. Duis non sem dolor. Pellentesque felis sem, facilisis eu tincidunt ac, ullamcorper sit amet turpis. Ut sit amet massa vitae ante sodales tempus at vel ipsum. Duis egestas rhoncus nibh, eget egestas eros tincidunt vitae. Proin ut luctus justo.';
			$array = explode(' ', $content);
			shuffle($array); 
			$content = implode(' ', $array); //p($content);
			$news->content = $content;
			$news->title = $array[array_rand($array, 1)]; 
			// $news->title = '101010101;';'
			$news->slug = $news->title; 
			$news->parent = 0;

			try {
			    $news->save();
			} catch (\Meagr\MeagrException $e) {
			    echo $e->getMessage() . "\n";
			    // \Meagr\Router::redirect('/news');
			}
				
		}	
	}

	public static function GET_delete($id = '') {
		if (trim($id) == '') {
			\Meagr\Router::redirect('/news/');
		}

		$rows = Modules\Models\News::init()->where('id', $id); 
		if ($rows) {
			$id = $rows->delete(); 
		}
		
		if ($id) {
			\Meagr\Router::redirect('/news/?deleted=ok');
		}
	}
}