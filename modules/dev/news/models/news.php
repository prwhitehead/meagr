<?

namespace Modules\News\Models;

class News extends \Meagr\Model {

	public static $table = 'news';
	
	public static $columns = array(
			'id' => array(
				'database' => array(
						'id' => 'id', 
						'type' => 'int(10)', 
						'primary' => true, 
						'auto_increment' => true, 
						'null' => false
					),
				'form' => array(
						'display' => true, 
						'title' => 'ID', 
						'type' => 'readonly',
					),
				'list' => array(
						'display' => true, 
						'title' => 'ID', 
					),
				'validate' => array()					
				),
			'new_id' => array(
				'database' => array(
						'id' => 'new_id', 
						'type' => 'int(10)', 
					),
				'form' => array(
						'display' => true, 
						'title' => 'ID', 
						'type' => 'readonly',
					),
				'list' => array(
						'display' => true, 
						'title' => 'ID', 
					),		
				'validate' => array()
				),		
			'title' => array(
				'database' => array(
						'id' => 'title', 
						'type' => 'varchar(255)', 
						'default' => '', 
						'null' => false
					),
				'form' => array(
						'display' => true, 
						'title' => 'Title', 
						'type' => 'text',
					),
				'list' => array(
						'display' => true, 
						'title' => 'Title', 
					),			
				'validate' => array('is_string', 'not_empty')					
				),
			'slug' => array(
				'database' => array(
						'id' => 'slug', 
						'type' => 'varchar(255)', 
						'default' => '', 
						'null' => false
					),
				'form' => array(
						'display' => true, 
						'title' => 'Title', 
						'type' => 'text',
					),
				'list' => array(
						'display' => true, 
						'title' => 'Title', 
					),	
				'validate' => array('create_slug')
				),
			'meta_title' => array(
				'database' => array(
						'id' => 'meta_title', 
						'type' => 'varchar(255)',
						'default' => '', 
						'null' => false
					),
				'form' => array(
						'display' => true, 
						'title' => 'Slug', 
						'type' => 'text',				
					),				
				'list' => array(
						'display' => true, 
						'title' => 'Slug', 
					),	
				//pass to function not_ and pass in 'empty'
				//pass to function valid_ and pass in 'url'
				'validate' => array('not_empty', 'valid_url')									
				),
			'meta_desc' => array(
				'database' => array(
						'id' => 'meta_desc', 
						'type' => 'varchar(255)',
						'default' => '', 
						'null' => false
					),
				'form' => array(
						'display' => true, 
						'title' => 'Slug', 
						'type' => 'text',				
					),				
				'list' => array(
						'display' => true, 
						'title' => 'Slug', 
					),	
				//pass to function not_ and pass in 'empty'
				//pass to function valid_ and pass in 'url'
				'validate' => array('not_empty', 'valid_url')									
				),		
			'content' => array(
				'database' => array(
						'id' => 'content', 
						'length' => '', 
						'type' => 'text',
						'default' => '', 
						'null' => true
					),
				'form' => array(
						'display' => true, 
						'title' => 'Slug', 
						'type' => 'text',
					),
				'list' => array(
						'display' => true, 
						'title' => 'Content', 
					),		
				'validate' => array()					

				),	
			'excerpt' => array(
				'database' => array(
						'id' => 'excerpt', 
						'length' => '', 
						'type' => 'text',
						'default' => '', 
						'null' => true
					),
				'form' => array(
						'display' => true, 
						'title' => 'Slug', 
						'type' => 'text',
					),
				'list' => array(
						'display' => true, 
						'title' => 'Content', 
					),	
				'validate' => array()					
				),				
			'parent' => array(
				'database' => array(
						'id' => 'parent', 
						'type' => 'int(10)',
						'default' => 0, 
						'null' => true
					),
				'form' => array(
						'display' => true, 
						'title' => 'Parent', 
						'type' => 'text',			
					),
				'list' => array(
						'display' => true, 
						'title' => 'Parent', 
					),				
				//pass to function is_ with value 'int'
				'validate' => array('is_int')						
				),	
			'created_at' => array(
				'database' => array(
						'id' => 'created_at', 
						'length' => '', 
						'type' => 'datetime',
						'default' => '0000-00-00 00:00:00', 
						'null' => false
					),
				'form' => array(
						'display' => false, 
						'title' => 'Created', 
						'type' => 'readonly'				
					),
				'list' => array(
						'display' => true, 
						'title' => 'Created', 
					),				
				),	
			'viewed_at' => array(
				'database' => array(
						'id' => 'viewed_at', 
						'length' => '', 
						'type' => 'datetime',
						'default' => '0000-00-00 00:00:00', 
						'null' => false
					),
				'form' => array(
						'display' => false, 
						'title' => 'Created', 
						'type' => 'readonly'				
					),
				'list' => array(
						'display' => true, 
						'title' => 'Created', 
					),				
				),				
			'updated_at' => array(
				'database' => array(
						'id' => 'updated_at', 
						'length' => '', 
						'type' => 'datetime',
						'default' => '0000-00-00 00:00:00', 
						'null' => false
					),
				'form' => array(
						'display' => false, 
						'title' => 'Last updated', 
						'type' => 'readonly',				
					),
				'list' => array(
						'display' => false, 
						'title' => 'Updated', 
					),				
				),																						
		);

	static function test() {
		p(__METHOD__);
	}
}