<?

/**
* Autoloader
*
*
* @package Meagr
* @version 1.0.0
* @author Paul Whitehead
*/

namespace Meagr; 

class Autoloader {

	public static function load($classname) {

	    $classname = ltrim($classname, '\\');
	    $filename  = '';
	    $namespace = '';

	    if ($lastNsPos = strrpos($classname, '\\')) {
	        $namespace = substr($classname, 0, $lastNsPos);
	        $classname = substr($classname, $lastNsPos + 1);
	    	$filename = SITE_PATH . '/' . strtolower(str_replace('\\', DS, $namespace) . DS);
	    }
	
	    $filename .= strtolower($classname) . '.php'; //p($filename);
	    if (file_exists($filename) and ! class_exists(basename($classname))) { 

			\Meagr\Debug::init('file')->add(array('message' => $filename,
												'class' => __FILE__, 
												'status' => 'success', 
												'backtrace' => \Meagr\Debug::backtrace()));
	    	
    		require_once $filename;	 
    		return;	     	
	    } 	

		\Meagr\Debug::init('file')->add(array('message' => $filename,
											'class' => __FILE__, 
											'status' => 'error', 
											'backtrace' => \Meagr\Debug::backtrace()));

		return $filename !== false;
	}
}

spl_autoload_register(__NAMESPACE__ . '\Autoloader::load');