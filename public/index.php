<? 

/*===== start editable settings =====*/

//define the domain
define('SITE_DOMAIN', 'meagr.com');

//default site url
define('SITE_URL', 'http://' . SITE_DOMAIN);

//the public url
define('PUBLIC_URL', SITE_URL);

//sets the application id, used for session variables etc
define('ID', 'meagr'); 

//are we in debug - if true, model will check db schema constantly and set all cache times to 0 (not caching anything)
define('IS_DEBUG', true);

//our current langauge code
define('LANG_CODE', 'EN');

//set the environment which is required - related to the config which is loaded
//this would be the same as the folder within your modules/config/ENVIRONMENT/
define('ENVIRONMENT', 'development'); 

//set the location of the sites root level
define('SITE_PATH', realpath(__DIR__ . '/..'));

//set the publically accessible directory 
define('PUBLIC_PATH', SITE_PATH . '/public');

//set the location and name of the modules folder
define('MODULE_PATH', SITE_PATH . '/modules');

//our vender folder name
define('VENDOR_PATH', SITE_PATH . '/vendor');

//set nonce info
define('MEAGR_NONCE_UNIQUE_KEY', '12jhsfg87897455780)(*&^%$£@£$%^&JHGFDSZXCjhdjhsgdfT^&^%$£@WERTYUJBVD');
define('MEAGR_NONCE_DURATION', 600); // 300 makes link or form good for 5 minutes from time of generation

/*===== stop editing settings, here is a unicorn =====*/

//                                     ~~*
//                                       ,/
//                                      //
//                               ~.%(\%// 
//                            ~~*%%%%  ^\
//                         ~~*%%%%%   (6 \ 
//                      ~~~*%%%%%    ,    \
//       ...._        ~~~*%%%%%     /"._  ,`,
//     ~*%%%%\\ ,...~~~*%%%%%%%    /    `-.-'
//    ~.*%%%  ;'    `"'"*%%%%@    (      
//   ~~*%%%' /                `@  \
//  ~~~*%%% |                   ``@| 
//   ~~*%%' |        .       '     |
//  ~~*%%   \     _ '      `t   ,.')
//  ~*%'    |   y;- -,-""'"-.\   \/  
// ~*%      / ./  ) /         `\  \
//         |./   ( (           / /'
//         ||     \\          //'|
//         ||      \\       _//'||
//         ||       ))     |_/  ||
//         \_\     |_/          ||
//         `'"                  \_\
         
                                                                     
//start our sessions
session_start();

// Encoding, force utf-8
mb_internal_encoding("UTF-8");
mb_http_output("UTF-8");

//error reporting
ini_set('display_errors', 1); 
error_reporting(E_ALL ^ E_NOTICE);

defined('MEAGR_MEMORY') or define('MEAGR_MEMORY', memory_get_usage(true));

//define our start time
list($usec, $sec) = explode(" ", microtime());
defined('MEAGR_TIME') or define('MEAGR_TIME', (float) $usec + (float) $sec);

//shorthand dir seporator
define('DS', DIRECTORY_SEPARATOR);


//also the composure autoloader
include_once VENDOR_PATH . '/autoload.php';

//include our base autoloader
include_once 'autoloader.php';


//start our app timer and logging now we have all our requird files
\Meagr\Debug::init('log')->add(array('message' => 'Loading Core',
									'class' => __FILE__, 
									'status' => 'success', 
									'backtrace' => Meagr\Debug::backtrace()));

//check for a subdomain before we load any configs etc
$array = parse_url('http://' . \Meagr\Input::server('http_host'));
$array['host'] = explode('.', $array['host']); 
if ($array['host'][0] !== 'www' and ! stripos(SITE_URL, $array['host'][0])) {
	define('SITE_SUBDOMAIN', $array['host'][0] . '\\');
} else {
	define('SITE_SUBDOMAIN', '');
}

//init our config routes
$router = Meagr\Router::init()
						// and process them into route objects
						->addRoutes()
						//and translate each route object from the current url
						->mapRoutes()
						//match all routes to Uri and class::method combo or set 404
						->matchRoutes()
						//check all matched routes for class/methods which exists
						->getMatchedRoute();

//pass our routes to the Response class
$response = Meagr\Response::init($router)
						//set how long the page should be cached for
						// ->cache(3600)
						//if empty set default headers
						->headers()
						//fill the body with the template and partials
						->body()
						//process any additional functions
						->filter(function($body){
							//do stuff
							$time = \Meagr\Timer::convert(round(Meagr\Timer::mtime() - MEAGR_TIME, 5));
							$body = str_replace('{apptime}', $time, $body);
							return $body;
						})
						//process any additional functions
						->filter(function($body){
							$body = str_replace('{file}', \Meagr\Debug::init('file')->output('success'), $body);

							return $body;
						})	
						->filter(function($body){
							$body = str_replace('{log}', \Meagr\Debug::init('log')->output(), $body);

							return $body;
						})												
						//return to screen
						->go();