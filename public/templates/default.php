<!DOCTYPE html>
    <!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"><![endif]-->
    <!--[if IE 8]><html class="no-js lt-ie9 ie8"><![endif]-->
    <!--[if gt IE 8]><!--><html class="no-js <?=\Meagr\Browser::init()->getBrowser(); ?>"><!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?=\Meagr\Meta::init()->title(); ?></title>
        <meta name="description" content="<?=\Meagr\Meta::init()->description(); ?>">
        <meta name="keywords" content="<?=\Meagr\Meta::init()->keywords(); ?>">
        <meta name="viewport" content="width=device-width">

        <? /*incude the css files in the cache file */ 
        $array = array(
            PUBLIC_PATH . "/css/bootstrap.css", 
            PUBLIC_PATH . "/css/main.css"
        ); ?>
        <link rel="stylesheet" href="<?=\Meagr\Cache::init('css')->concat($array)->file ;?>">

        <!-- fix old browsers -->
        <script src="<?=PUBLIC_URL; ?>/js/vendor/modernizr-2.6.2.min.js"></script>          
    </head>
    <body>
        <? $string = \Meagr\Hook::trigger('header'); ?>
        <div id="wrapper">
            <?=\Meagr\View::partial('header'); ?>
            <?//output the content, passed to the View::template() method ?>
            <?=$content; ?>
            <?=\Meagr\View::partial('footer'); ?>
        </div>
        
        <div id="timer">
            <p>Script executed in {apptime}</p>
            <p>App used <?=\Meagr\Debug::init()->appMemory(); ?> of memory</p>
        </div>     
    </body>

    <? /*incude the javascript files in the cache file */ 
    $array = array(
        PUBLIC_PATH . "/js/vendor/jquery-1.8.2.js", 
        PUBLIC_PATH . "/js/main.js"
    ); ?>
    <script src="<?=\Meagr\Cache::init('js')->concat($array)->file; ?>"></script>      
</html>